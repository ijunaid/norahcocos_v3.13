#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    /////////////////////////////
    // 2. add a menu item with "X" image, which is clicked to quit the program
    //    you may modify it.

    // add a "close" icon to exit the progress. it's an autorelease object
    auto closeItem = MenuItemImage::create(
                                           "CloseNormal.png",
                                           "CloseSelected.png",
                                           CC_CALLBACK_1(HelloWorld::menuCloseCallback, this));
    
    closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
                                origin.y + closeItem->getContentSize().height/2));

    // create menu, it's an autorelease object
    auto menu = Menu::create(closeItem, NULL);
    menu->setPosition(Vec2::ZERO);
    this->addChild(menu, 1);

    /////////////////////////////
    // 3. add your codes below...

    // add a label shows "Hello World"
    // create and initialize a label
    
//    auto label = Label::createWithTTF("Hello World", "fonts/Marker Felt.ttf", 24);
//    
//    // position the label on the center of the screen
//    label->setPosition(Vec2(origin.x + visibleSize.width/2,
//                            origin.y + visibleSize.height - label->getContentSize().height));
//
//    // add the label as a child to this layer
//    this->addChild(label, 1);

    // add "HelloWorld" splash screen"
    auto sprite = Sprite::create("HelloWorld.png");

    // position the sprite on the center of the screen
    sprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));

    // add the sprite as a child to this layer
    this->addChild(sprite, 0);
    
    _norahAnalytics = new NorahAnalytics();
    
    auto menuItem1 = MenuItemFont::create("game_start", [this](Ref * obj){
        _norahAnalytics->handleEvent(NorahAnalytics::DB_TYPE::kUserBehaviour, "game_start");
    });
    auto menuItemWidth  = closeItem->getContentSize().width + 5;
    auto menuItemHeight = closeItem->getContentSize().height;
    menuItem1->setAnchorPoint(Vec2(0.0f,0.5f));
    menuItem1->setPosition(Vec2(origin.x + menuItemWidth,origin.y + visibleSize.height - menuItemHeight));
    
    auto menuItem2 = MenuItemFont::create("play_start", [this](Ref * obj){
        _norahAnalytics->handleEvent(NorahAnalytics::DB_TYPE::kUserBehaviour, "play_start");
    });
    menuItem2->setAnchorPoint(Vec2(1.0f,0.5f));
    menuItem2->setPosition(Vec2(origin.x + visibleSize.width - menuItemWidth,origin.y + visibleSize.height - menuItemHeight));
    
    auto menuItem3 = MenuItemFont::create("engaged", [this](Ref * obj){
        _norahAnalytics->handleEvent(NorahAnalytics::DB_TYPE::kUserBehaviour, "engaged");
    });
    menuItem3->setAnchorPoint(Vec2(0.0f,0.5f));
    menuItem3->setPosition(Vec2(origin.x + menuItemWidth,origin.y + visibleSize.height - menuItemHeight*3));
    
    auto menuItem4 = MenuItemFont::create("game_paused", [this](Ref * obj){
        _norahAnalytics->handleEvent(NorahAnalytics::DB_TYPE::kUserBehaviour, "game_paused");
    });
    menuItem4->setAnchorPoint(Vec2(1.0f,0.5f));
    menuItem4->setPosition(Vec2(origin.x + visibleSize.width - menuItemWidth,origin.y + visibleSize.height - menuItemHeight*3));
    
    auto menuItem5 = MenuItemFont::create("player_fail", [this](Ref * obj){
        _norahAnalytics->handleEvent(NorahAnalytics::DB_TYPE::kUserBehaviour, "player_fail");
    });
    menuItem5->setAnchorPoint(Vec2(0.0f,0.5f));
    menuItem5->setPosition(Vec2(origin.x + menuItemWidth,origin.y + visibleSize.height - menuItemHeight*5));
    
    auto menuItem6 = MenuItemFont::create("play_end", [this](Ref * obj){
        _norahAnalytics->handleEvent(NorahAnalytics::DB_TYPE::kUserBehaviour, "play_end");
    });
    menuItem6->setAnchorPoint(Vec2(1.0f,0.5f));
    menuItem6->setPosition(Vec2(origin.x + visibleSize.width - menuItemWidth,origin.y + visibleSize.height - menuItemHeight*5));
    auto menuItem7 = MenuItemFont::create("game_end", [this](Ref * obj){
        _norahAnalytics->handleEvent(NorahAnalytics::DB_TYPE::kUserBehaviour, "game_end");
    });
    menuItem7->setAnchorPoint(Vec2(0.0f,0.5f));
    menuItem7->setPosition(Vec2(origin.x + menuItemWidth,origin.y + visibleSize.height - menuItemHeight*7));
    
    auto menuItem8 = MenuItemFont::create("store_opened", [this](Ref * obj){
        _norahAnalytics->handleEvent(NorahAnalytics::DB_TYPE::kUserBehaviour, "store_opened");
    });
    menuItem8->setAnchorPoint(Vec2(1.0f,0.5f));
    menuItem8->setPosition(Vec2(origin.x + visibleSize.width - menuItemWidth,origin.y + visibleSize.height - menuItemHeight*7));
    auto menuItem9 = MenuItemFont::create("store_closed", [this](Ref * obj){
        _norahAnalytics->handleEvent(NorahAnalytics::DB_TYPE::kUserBehaviour, "store_closed");
    });
    menuItem9->setAnchorPoint(Vec2(0.0f,0.5f));
    menuItem9->setPosition(Vec2(origin.x + menuItemWidth,origin.y + visibleSize.height - menuItemHeight*9));
    
    
    //Handle event UserPurchase
    auto menuItem10 = MenuItemFont::create("UserPurchase", [this](Ref * obj){
        _norahAnalytics->handleEvent(NorahAnalytics::DB_TYPE::kUserPurchase, "product_id");
    });
    menuItem10->setAnchorPoint(Vec2(1.0f,0.5f));
    menuItem10->setPosition(Vec2(origin.x + visibleSize.width - menuItemWidth,origin.y + visibleSize.height - menuItemHeight*9));
    
    //Handle event UserBehaviour
    auto menuItem11 = MenuItemFont::create("UserBehaviour", [this](Ref * obj){
        _norahAnalytics->handleEvent(NorahAnalytics::DB_TYPE::kVirtualPurchase, "productName","amount");
    });
    menuItem11->setAnchorPoint(Vec2(0.0f,0.5f));
    menuItem11->setPosition(Vec2(origin.x + menuItemWidth,origin.y + visibleSize.height - menuItemHeight*11));
    
    
    
    
    Menu *menu1 = Menu::create( menuItem1, menuItem2, menuItem3, menuItem4, menuItem5, menuItem6, menuItem7, menuItem8, menuItem9, menuItem10, menuItem11, NULL );
    
    menu1->setPosition(Vec2::ZERO);
    
    this->addChild(menu1, 0);
    
    return true;
}


void HelloWorld::menuCloseCallback(Ref* pSender)
{
    //Close the cocos2d-x game scene and quit the application
    Director::getInstance()->end();

    #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
    exit(0);
#endif
    
    /*To navigate back to native iOS screen(if present) without quitting the application  ,do not use Director::getInstance()->end() and exit(0) as given above,instead trigger a custom event created in RootViewController.mm as below*/
    
    //EventCustom customEndEvent("game_scene_close_event");
    //_eventDispatcher->dispatchEvent(&customEndEvent);
    
    
}
