//
//  ConnectNative.h
//  Casino
//
//  Created by user  on 2015/03/30.
//
//

#ifndef __Nyanko__ConnectNative__
#define __Nyanko__ConnectNative__

#include "cocos2d.h"
#include <stddef.h>

namespace native_plugin {
    class ConnectNative
    {
    public:
        
        /* Get an unique identifier of the device.
         - on iOS, it uses identifierForVendor, which CAN change if all apps are uninstalled at once then reinstalled. It can also be null at the beginning. If it is, retry later
         - on Android, it uses ANDROID_IT, which CAN change on factory reset and can be different if there are several accounts on the device.
         */
        static std::string getUniqueIdentifier();
        
        static std::string getDeviceModel();
    };
    
} // end of namespace Cocos2dExt
#endif /* defined(__Nyanko__ConnectNative__) */
