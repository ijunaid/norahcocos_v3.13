#include "NorahAnalytics.h"
#include "../../cocos2d/external/json/rapidjson.h"
#include "../../cocos2d/external/json/document.h"
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
#include "../cocos2d/external/curl/include/ios/curl/curl.h"
#elif CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
#include "../cocos2d/external/curl/include/android/curl/curl.h"
#endif

#ifdef ENABLE_LOG
#include "HelloWorldScene.h"
#endif

using namespace rapidjson;
using namespace cocos2d;

#define GAME_ID             "59cb00c0-3d7c-4f91-86c0-0920db71b8d1"
#define GAME_VERSION_ID     "125c0d90-4d1d-40da-aa66-ebda8e527321"

#define MAX_ENTRY           5
#define LIMIT_FILE_SIZE     5000000   // 5MB

#define NAME_DB_TEMP                "temp.db"
#define NAME_DB_BACKUP              "backup.db"
#define NAME_TABLE_USERBEHAVIER     "userbehaviour"
#define NAME_TABLE_USERPURCHASE     "userpurchase"
#define NAME_TABLE_VIRTUALPURCHASE  "virtualpurchase"
#define NAME_TABLE_USERINFO         "user_info"

#define TAG_DB_USERBEHAVIER_BK      "TAG_DB_USERBEHAVIER_BK"
#define TAG_DB_USERPURCHASE_BK      "TAG_DB_USERPURCHASE_BK"
#define TAG_DB_VIRTUALPURCHASE_BK   "TAG_DB_VIRTUALPURCHASE_BK"

#define TAG_DB_USERBEHAVIER         "TAG_DB_USERBEHAVIER"
#define TAG_DB_USERPURCHASE         "TAG_DB_USERPURCHASE"
#define TAG_DB_VIRTUALPURCHASE      "TAG_DB_VIRTUALPURCHASE"

NorahAnalytics::NorahAnalytics():
#ifdef ENABLE_LOG
_logger(nullptr),
#endif
_isDB_UserBehavierLocked(false),
_isDB_UserPurchaseLocked(false),
_isDB_VirtualPurchaseLocked(false),
_isDB_UserLocked(false),
_playerID(""),
_type(DB_TYPE::kNone)
{
    cocos2d::network::HttpClient::getInstance()->setTimeoutForConnect(5000);
    createDatabase(NorahAnalytics::DB_TYPE::kUserBehaviour);
    createDatabase(NorahAnalytics::DB_TYPE::kUserBehaviour_Backup);
    createDatabase(NorahAnalytics::DB_TYPE::kUserPurchase);
    createDatabase(NorahAnalytics::DB_TYPE::kUserPurchase_Backup);
    createDatabase(NorahAnalytics::DB_TYPE::kVirtualPurchase);
    createDatabase(NorahAnalytics::DB_TYPE::kVirtualPurchase_Backup);
    createDatabase(NorahAnalytics::DB_TYPE::kUserInfo);
    
    _devideID = native_plugin::ConnectNative::getDeviceModel();
    _playerID = getPlayerIdFromDatabase();
    CCLOG("===path %s",FileUtils::getInstance()->getWritablePath().c_str());
    CCLOG("===devide model %s", _devideID.c_str());
}

bool NorahAnalytics::isInternetConnected() {
    CURL *curl;
    CURLcode res;
    curl = curl_easy_init();
    if (curl) {
        curl_easy_setopt(curl, CURLOPT_URL, "http://www.google.com");
        res = curl_easy_perform(curl);
        curl_easy_cleanup(curl);
        if (res == 0)
            return true;
        else
            return false;
    }
    else
        return false;
}

NorahAnalytics::~NorahAnalytics(){
}

std::string NorahAnalytics::getCurrentTime(){
    std::time_t now= std::time(0);
    std::tm* now_tm= std::gmtime(&now);
    char buf[20];
    std::strftime(buf, sizeof(buf), "%Y-%m-%d %H:%M:%S", now_tm);
    std::string str(buf);
    return str;
}

void NorahAnalytics::postBackupData(DB_TYPE type, const std::string &_data){
    if (_data.size() > 0){
        
        std::string url = "";
        std::string tag = "";
        std::string tableName = "";
        if (type == DB_TYPE::kUserBehaviour_Backup){
            url.append(StringUtils::format("http://norahpricing.com/user_behaviour"));
            tag.append(TAG_DB_USERBEHAVIER_BK);
            tableName.append("user_behaviour");
        }else if (type == DB_TYPE::kUserPurchase_Backup){
            url.append(StringUtils::format("http://norahpricing.com/user_purchases"));
            tag.append(TAG_DB_USERPURCHASE_BK);
            tableName.append("user_purchases");
        }else if (type == DB_TYPE::kVirtualPurchase_Backup){
            url.append(StringUtils::format("http://norahpricing.com/virtual_purchases"));
            tag.append(TAG_DB_VIRTUALPURCHASE_BK);
            tableName.append("virtual_purchases");
        }
        
#ifdef ENABLE_LOG
        _logger->setLog(StringUtils::format( "Request: POST table[%s] in backup.db to server",tableName.c_str()).c_str());
#endif
        
        std::vector<std::string> header;
        header.push_back("Content-Type:application/json");
        
        cocos2d::network::HttpRequest* request = new cocos2d::network::HttpRequest();
        request->setUrl(url.c_str());
        request->setHeaders(header);
        request->setRequestType(cocos2d::network::HttpRequest::Type::POST);
        request->setResponseCallback(CC_CALLBACK_2(NorahAnalytics::onHttpPostBackupCallback, this));
        
        std::string tmp = "{\"gameId\":\"%s\",\"playerId\":\"%s\",\"gameVersionId\":\"%s\",\"data\":%s}";
        std::string content = StringUtils::format(tmp.c_str(),GAME_ID, _playerID.c_str(),GAME_VERSION_ID, _data.c_str());
        
        request->setRequestData(content.c_str(), strlen(content.c_str()));
        
        request->setTag(tag);
        cocos2d::network::HttpClient::getInstance()->send(request);
        request->release();
    }
}

void NorahAnalytics::onHttpPostBackupCallback(HttpClient *sender, HttpResponse *response){
    std::vector<char> *buffer = response->getResponseData();
    std::string s(buffer->begin(), buffer->end());
    
    auto strTag = response->getHttpRequest()->getTag();
    
    //POST FAILED
    if (!response || !response->isSucceed())
    {
#ifdef ENABLE_LOG
        _logger->setLog("Response: POST table in backup.db FAILED");
#endif
        return;
    }
    
    //POST SUCCESSFULLY
#ifdef ENABLE_LOG
    if ( strcmp(strTag, TAG_DB_USERBEHAVIER_BK) == 0 ){
        _isDB_UserBehavierLocked = false;
        _logger->setLog("Response: POST table[user_behaviour] in backup.db successfully");
    }else if ( strcmp(strTag, TAG_DB_USERPURCHASE_BK) == 0 ){
        _isDB_UserPurchaseLocked = false;
        _logger->setLog("Response: POST table[user_purchase] in backup.db successfully");
    }else if ( strcmp(strTag, TAG_DB_VIRTUALPURCHASE_BK) == 0 ){
        _isDB_VirtualPurchaseLocked = false;
        _logger->setLog("Response: POST table[virtual_purchase] in backup.db successfully");
    }
#endif
    DB_TYPE type = DB_TYPE::kNone;
    if ( strcmp(strTag, TAG_DB_USERBEHAVIER_BK) == 0 ){
        type = DB_TYPE::kUserBehaviour_Backup;
    }else if ( strcmp(strTag, TAG_DB_USERPURCHASE_BK) == 0 ){
        type = DB_TYPE::kUserPurchase_Backup;
    }else if ( strcmp(strTag, TAG_DB_VIRTUALPURCHASE_BK) == 0 ){
        type = DB_TYPE::kVirtualPurchase_Backup;
    }
    
    //clear backup data
    clearDatabase(type);
}

void NorahAnalytics::postData(DB_TYPE type, const std::string &_data){
    if (_data.size() > 0){
        
        std::string url = "";
        std::string tag = "";
        std::string tableName = "";
        if (type == DB_TYPE::kUserBehaviour){
            _isDB_UserBehavierLocked = true;
            url.append(StringUtils::format("http://norahpricing.com/user_behaviour"));
            tag.append(TAG_DB_USERBEHAVIER);
            tableName.append("user_behaviour");
        }else if (type == DB_TYPE::kUserPurchase){
            url.append(StringUtils::format("http://norahpricing.com/user_purchases"));
            tag.append(TAG_DB_USERPURCHASE);
            tableName.append("user_purchases");
            _isDB_UserPurchaseLocked = true;
        }else if (type == DB_TYPE::kVirtualPurchase){
            url.append(StringUtils::format("http://norahpricing.com/virtual_purchases"));
            tag.append(TAG_DB_VIRTUALPURCHASE);
            tableName.append("virtual_purchases");
            _isDB_VirtualPurchaseLocked = true;
        }
        
#ifdef ENABLE_LOG
        _logger->setLog(StringUtils::format( "Request: POST table[%s] in temp.db to server",tableName.c_str() ).c_str());
#endif
        
        std::vector<std::string> header;
        header.push_back("Content-Type:application/json");
        
        
        cocos2d::network::HttpRequest* request = new cocos2d::network::HttpRequest();
        request->setUrl(url.c_str());
        request->setHeaders(header);
        request->setRequestType(cocos2d::network::HttpRequest::Type::POST);
        request->setResponseCallback(CC_CALLBACK_2(NorahAnalytics::onHttpPostCallback, this));
        
        std::string tmp = "{\"gameId\":\"%s\",\"playerId\":\"%s\",\"gameVersionId\":\"%s\",\"data\":%s}";
        std::string content = StringUtils::format(tmp.c_str(),GAME_ID, _playerID.c_str(),GAME_VERSION_ID, _data.c_str());
        
        request->setRequestData(content.c_str(), strlen(content.c_str()));
        
        request->setTag(tag);
        cocos2d::network::HttpClient::getInstance()->send(request);
        request->release();
    }
}

void NorahAnalytics::onHttpPostCallback(HttpClient *sender, HttpResponse *response){
    std::vector<char> *buffer = response->getResponseData();
    std::string s(buffer->begin(), buffer->end());
    
    auto strTag = response->getHttpRequest()->getTag();
    std::string dbPath = FileUtils::getInstance()->getWritablePath();
    
    //POST FAILED
    if (!response || !response->isSucceed() || s.empty())
    {
#ifdef ENABLE_LOG
        _logger->setLog("Receive respone: POST temp.db failed");
#endif
        //calculate backup size
        long sizeBackup =  FileUtils::getInstance()->getFileSize(dbPath + NAME_DB_BACKUP);
        if ( strcmp(strTag, TAG_DB_USERBEHAVIER) == 0 ){
            _isDB_UserBehavierLocked = false;
        }else if ( strcmp(strTag, TAG_DB_USERPURCHASE) == 0 ){
            _isDB_UserPurchaseLocked = false;
        }else if ( strcmp(strTag, TAG_DB_VIRTUALPURCHASE) == 0 ){
            _isDB_VirtualPurchaseLocked = false;
        }
        if (sizeBackup >= LIMIT_FILE_SIZE){
            //discard entry
            if ( strcmp(strTag, TAG_DB_USERBEHAVIER) == 0 && _unhandleEntryUserBehavier.size() > 0){
                _unhandleEntryUserBehavier.pop_front();
            }else if ( strcmp(strTag, TAG_DB_USERPURCHASE) == 0 && _unhandleEntryUserPurchase.size() > 0){
                _unhandleEntryUserPurchase.pop_front();
            }else if ( strcmp(strTag, TAG_DB_VIRTUALPURCHASE) == 0 && _entryVirtualPurchase.size() > 0){
                _entryVirtualPurchase.pop_front();
            }
        }else{
            //save entry to backup db
            if ( strcmp(strTag, TAG_DB_USERBEHAVIER) == 0 && _unhandleEntryUserBehavier.size() > 0){
                std::vector<std::string> entry = _unhandleEntryUserBehavier[0];
                saveEntryToDB(DB_TYPE::kUserBehaviour_Backup, entry[0].c_str(), entry[1].c_str());
#ifdef ENABLE_LOG
                _logger->setLog(StringUtils::format("Save entry(%s, %s) to backup.db", entry[0].c_str(), entry[1].c_str()).c_str());
#endif
                _unhandleEntryUserBehavier.pop_front();
            }else if ( strcmp(strTag, TAG_DB_USERPURCHASE) == 0 && _unhandleEntryUserPurchase.size() > 0){
                std::vector<std::string> entry = _unhandleEntryUserPurchase[0];
                saveEntryToDB(DB_TYPE::kUserPurchase_Backup, entry[0].c_str(), entry[1].c_str());
#ifdef ENABLE_LOG
                _logger->setLog(StringUtils::format("Save entry(%s, %s) to backup.db", entry[0].c_str(), entry[1].c_str()).c_str());
#endif
                _unhandleEntryUserPurchase.pop_front();
            }else if ( strcmp(strTag, TAG_DB_VIRTUALPURCHASE) == 0 && _entryVirtualPurchase.size() > 0){
                std::vector<std::string> entry = _entryVirtualPurchase[0];
                saveEntryToDB(DB_TYPE::kVirtualPurchase_Backup,
                              entry[0].c_str(), entry[1].c_str(), entry[2].c_str());
                
#ifdef ENABLE_LOG
                _logger->setLog(StringUtils::format("Save entry(%s, %s, %s) to backup.db", entry[0].c_str(), entry[1].c_str(), entry[2].c_str()).c_str());
#endif
                _entryVirtualPurchase.pop_front();
            }
        }
        
        return;
    }
    
#ifdef ENABLE_LOG
    if ( strcmp(strTag, TAG_DB_USERBEHAVIER) == 0 ){
        _isDB_UserBehavierLocked = false;
        _logger->setLog("Response: POST table[user_behaviour] in temp.db successfully");
    }else if ( strcmp(strTag, TAG_DB_USERPURCHASE) == 0 ){
        _isDB_UserPurchaseLocked = false;
        _logger->setLog("Response: POST table[user_purchase] in temp.db successfully");
    }else if ( strcmp(strTag, TAG_DB_VIRTUALPURCHASE) == 0 ){
        _isDB_VirtualPurchaseLocked = false;
        _logger->setLog("Response: POST table[virtual_purchase] in temp.db successfully");
    }
#endif
    //POST SUCCESSFULLY
    //get No row in backup database
    DB_TYPE type = DB_TYPE::kNone;
    DB_TYPE typeBK = DB_TYPE::kNone;
    if ( strcmp(strTag, TAG_DB_USERBEHAVIER) == 0 ){
        type = DB_TYPE::kUserBehaviour;
        typeBK = DB_TYPE::kUserBehaviour_Backup;
        _unhandleEntryUserBehavier.clear();
    }else if ( strcmp(strTag, TAG_DB_USERPURCHASE) == 0 ){
        type = DB_TYPE::kUserPurchase;
        typeBK = DB_TYPE::kUserPurchase_Backup;
        _unhandleEntryUserPurchase.clear();
    }else if ( strcmp(strTag, TAG_DB_VIRTUALPURCHASE) == 0 ){
        type = DB_TYPE::kVirtualPurchase;
        typeBK = DB_TYPE::kVirtualPurchase_Backup;
        _entryVirtualPurchase.clear();
    }
    
    if (getNumRowInTable(typeBK) >= 1){
        //post entry from backup to server
        postBackupData(typeBK, getEntriesFromDatabase(typeBK));
    }
    
    
    //FINALLY,CLEAR DB ENTRY
#ifdef ENABLE_LOG
    _logger->setLog("Clear table in temp.db");
#endif
    clearDatabase(type);
}

void NorahAnalytics::requestPlayerId(){
    if (_isDB_UserLocked)
        return;
    _isDB_UserLocked = true;
    
#ifdef ENABLE_LOG
    _logger->setLog("Request: Get PlayerID");
#endif
    std::string url = "";
    std::string tag = "";
    url.append(StringUtils::format("http://35.185.183.47:4567/v1/player"));
    
    std::vector<std::string> header;
    header.push_back("Content-Type:application/json");
    
    
    cocos2d::network::HttpRequest* request = new cocos2d::network::HttpRequest();
    request->setUrl(url.c_str());
    request->setHeaders(header);
    request->setRequestType(cocos2d::network::HttpRequest::Type::POST);
    request->setResponseCallback(CC_CALLBACK_2(NorahAnalytics::onHttprequestPlayerIdCallback, this));
    
    std::string content = "{\"country\": \"default\",\"gender\": \"default\"}";
    
    request->setRequestData(content.c_str(), strlen(content.c_str()));
    request->setTag(tag);
    cocos2d::network::HttpClient::getInstance()->send(request);
    request->release();
}

void NorahAnalytics::onHttprequestPlayerIdCallback(HttpClient *sender, HttpResponse *response){
    std::vector<char> *buffer = response->getResponseData();
    std::string s(buffer->begin(), buffer->end());
    
    //POST FAILED
    if (!response || !response->isSucceed())
    {
#ifdef ENABLE_LOG
        _logger->setLog("Get PlayerID Failed");
        _logger->setLog("Save entries from temp.db to backup.db");
#endif
        moveEntriesFromTempToBackup();
        _isDB_UserLocked = false;
    }
    //POST CUCCESS
    else{
        Document doc;
        doc.Parse(s.c_str());
        _playerID = doc["id"].GetString();
#ifdef ENABLE_LOG
        _logger->setLog(StringUtils::format("Response: Get PlayerID: %s", _playerID.c_str()).c_str());
#endif
        
        std::string gameId = GAME_ID;
        std::string systemInfo = _devideID;
        std::string timestamp = getCurrentTime();
        saveUserInfoToDB(gameId.c_str(), _playerID.c_str(), systemInfo.c_str(), timestamp.c_str());
        postUserInfo(gameId.c_str(),_playerID.c_str(), systemInfo.c_str(), timestamp.c_str());
        _isDB_UserLocked = false;
        //for testing
        getPlayerIdFromDatabase();
        postTempDB();
    }
    
}

void NorahAnalytics::postUserInfo(const char *gameId, const char *playerID, const char *system_info, const char *timestamp){
#ifdef ENABLE_LOG
    _logger->setLog("Post User info");
#endif
    std::string url = "";
    std::string tag = "";
    url.append("http://norahpricing.com/user_info");
    
    std::vector<std::string> header;
    header.push_back("Content-Type:application/json");
    
    
    cocos2d::network::HttpRequest* request = new cocos2d::network::HttpRequest();
    request->setUrl(url.c_str());
    request->setHeaders(header);
    request->setRequestType(cocos2d::network::HttpRequest::Type::POST);
    
    std::string tmp = "{\"gameId\":\"%s\",\"playerId\":\"%s\",\"gameVersionId\":\"%s\",\"system_info\":\"%s\",\"timestamp\":\"%s\"}";
    std::string content = StringUtils::format(tmp.c_str(), gameId, playerID,GAME_VERSION_ID ,system_info, timestamp);
    request->setRequestData(content.c_str(), strlen(content.c_str()));
    request->setTag(tag);
    cocos2d::network::HttpClient::getInstance()->send(request);
    request->release();
}

void NorahAnalytics::createDatabase(DB_TYPE type){
    std::string dbPath = FileUtils::getInstance()->getWritablePath();
    std::string tableName = "";
    std::string strSQL = "";
    if (type == DB_TYPE::kUserBehaviour){
        dbPath.append(NAME_DB_TEMP);
        tableName.append(NAME_TABLE_USERBEHAVIER);
        strSQL.append("create table userbehaviour(mode text,timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP)");
    }else if ( type == DB_TYPE::kUserBehaviour_Backup ){
        dbPath.append(NAME_DB_BACKUP);
        tableName.append(NAME_TABLE_USERBEHAVIER);
        strSQL.append("create table userbehaviour(mode text,timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP)");
    }
    
    else if ( type == DB_TYPE::kUserPurchase ){
        dbPath.append(NAME_DB_TEMP);
        tableName.append(NAME_TABLE_USERPURCHASE);
        strSQL.append("create table userpurchase(productId text,timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP)");
    }else if ( type == DB_TYPE::kUserPurchase_Backup ){
        dbPath.append(NAME_DB_BACKUP);
        tableName.append(NAME_TABLE_USERPURCHASE);
        strSQL.append("create table userpurchase(productId text,timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP)");
    }
    
    else if ( type == DB_TYPE::kVirtualPurchase ){
        dbPath.append(NAME_DB_TEMP);
        tableName.append(NAME_TABLE_VIRTUALPURCHASE);
        strSQL.append("create table virtualpurchase(product_name text,amount text,timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP)");
    }else if ( type == DB_TYPE::kVirtualPurchase_Backup ){
        dbPath.append(NAME_DB_BACKUP);
        tableName.append(NAME_TABLE_VIRTUALPURCHASE);
        strSQL.append("create table virtualpurchase(product_name text,amount text,timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP)");
    }
    
    else if ( type == DB_TYPE::kUserInfo ){
        dbPath.append(NAME_DB_TEMP);
        tableName.append(NAME_TABLE_USERINFO);
        strSQL.append("create table user_info(gameId text,playerId text,system_info text,timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP)");
    }
    
    sqlite3 *pdb = nullptr;
    sqlite3_open(dbPath.c_str(),&pdb);
    int result = sqlite3_exec(pdb,strSQL.c_str(),NULL,NULL,NULL);
    
    sqlite3_close(pdb);
}

void NorahAnalytics::clearDatabase(DB_TYPE type){
    std::string dbPath = FileUtils::getInstance()->getWritablePath();
    std::string tableName = "";
    std::string strSQL = "";
    if (type == DB_TYPE::kUserBehaviour){
        dbPath.append(NAME_DB_TEMP);
        tableName.append(NAME_TABLE_USERBEHAVIER);
        strSQL.append( StringUtils::format("delete from %s",NAME_TABLE_USERBEHAVIER) );
        _isDB_UserBehavierLocked = false;
    }else if ( type == DB_TYPE::kUserBehaviour_Backup ){
        dbPath.append(NAME_DB_BACKUP);
        tableName.append(NAME_TABLE_USERBEHAVIER);
        strSQL.append( StringUtils::format("delete from %s",NAME_TABLE_USERBEHAVIER) );
    }
    
    else if (type == DB_TYPE::kUserPurchase){
        dbPath.append(NAME_DB_TEMP);
        tableName.append(NAME_TABLE_USERPURCHASE);
        strSQL.append( StringUtils::format("delete from %s",NAME_TABLE_USERPURCHASE) );
        _isDB_UserPurchaseLocked = false;
    }else if ( type == DB_TYPE::kUserPurchase_Backup ){
        dbPath.append(NAME_DB_BACKUP);
        tableName.append(NAME_TABLE_USERPURCHASE);
        strSQL.append( StringUtils::format("delete from %s",NAME_TABLE_USERPURCHASE) );
    }
    
    else if (type == DB_TYPE::kVirtualPurchase){
        dbPath.append(NAME_DB_TEMP);
        tableName.append(NAME_TABLE_VIRTUALPURCHASE);
        strSQL.append( StringUtils::format("delete from %s",NAME_TABLE_VIRTUALPURCHASE) );
        _isDB_VirtualPurchaseLocked = false;
    }else if ( type == DB_TYPE::kVirtualPurchase_Backup ){
        dbPath.append(NAME_DB_BACKUP);
        tableName.append(NAME_TABLE_VIRTUALPURCHASE);
        strSQL.append( StringUtils::format("delete from %s",NAME_TABLE_VIRTUALPURCHASE) );
    }
    
    else if ( type == DB_TYPE::kUserInfo ){
        dbPath.append(NAME_DB_TEMP);
        tableName.append(NAME_TABLE_USERINFO);
        strSQL.append( StringUtils::format("delete from %s",NAME_TABLE_USERINFO));
    }
    
    sqlite3 *pdb = nullptr;
    sqlite3_open(dbPath.c_str(),&pdb);
    sqlite3_exec(pdb,strSQL.c_str(),NULL,NULL,NULL);
    sqlite3_close(pdb);
    //CCLOG("===try clear %s", dbPath.c_str());
}


std::string NorahAnalytics::getEntriesFromDatabase(DB_TYPE type){
    std::string _data;
    std::string dbPath = FileUtils::getInstance()->getWritablePath();
    std::string tableName = "";
    if (type == DB_TYPE::kUserBehaviour){
        dbPath.append(NAME_DB_TEMP);
        tableName.append(NAME_TABLE_USERBEHAVIER);
    }else if (type == DB_TYPE::kUserBehaviour_Backup){
        dbPath.append(NAME_DB_BACKUP);
        tableName.append(NAME_TABLE_USERBEHAVIER);
    }
    
    else if (type == DB_TYPE::kUserPurchase){
        dbPath.append(NAME_DB_TEMP);
        tableName.append(NAME_TABLE_USERPURCHASE);
    }else if (type == DB_TYPE::kUserPurchase_Backup){
        dbPath.append(NAME_DB_BACKUP);
        tableName.append(NAME_TABLE_USERPURCHASE);
    }
    
    else if (type == DB_TYPE::kVirtualPurchase){
        dbPath.append(NAME_DB_TEMP);
        tableName.append(NAME_TABLE_VIRTUALPURCHASE);
    }else if (type == DB_TYPE::kVirtualPurchase_Backup){
        dbPath.append(NAME_DB_BACKUP);
        tableName.append(NAME_TABLE_VIRTUALPURCHASE);
    }
    
    else if (type == DB_TYPE::kUserInfo){
        dbPath.append(NAME_DB_TEMP);
        tableName.append(NAME_TABLE_USERINFO);
    }
    
    sqlite3 *pdb=NULL;
    sqlite3_open(dbPath.c_str(),&pdb);
    
    std::string strSQL = StringUtils::format("select * from %s", tableName.c_str());
    char **re;
    int r,c;
    sqlite3_get_table(pdb,strSQL.c_str(),&re,&r,&c,NULL);
    
    //parse header
    std::vector<std::string> header;
    for (int col = 0; col < c; col++){
        header.push_back(re[col]);
    }
    
    //parse content
    _data.clear();
    _data.append("[");
    for ( int row = 1; row <= r; row++ ){
        _data.append("{");
        for (int col = 0; col < c; col++){
            std::string tmp = "\"" + header.at(col) + "\"" + ":" + "\"" + re[c*row + col] + "\"" + ",";
            _data.append(tmp);
            CCLOG("====%s", tmp.c_str());
        }
        _data.pop_back(); // remove last ","
        _data.append("},");
    }
    _data.pop_back();//remove last ","
    _data.append("]");
    
    sqlite3_close(pdb);
    
    return _data;
}

int NorahAnalytics::getNumRowInTable(DB_TYPE type){
    std::string dbPath = FileUtils::getInstance()->getWritablePath();
    std::string tableName = "";
    if ( type == NorahAnalytics::DB_TYPE::kUserBehaviour ){
        dbPath.append(NAME_DB_TEMP);
        tableName.append(NAME_TABLE_USERBEHAVIER);
    }else if ( type == NorahAnalytics::DB_TYPE::kUserBehaviour_Backup ){
        dbPath.append(NAME_DB_BACKUP);
        tableName.append(NAME_TABLE_USERBEHAVIER);
    }
    
    else if ( type == NorahAnalytics::DB_TYPE::kUserPurchase ){
        dbPath.append(NAME_DB_TEMP);
        tableName.append(NAME_TABLE_USERPURCHASE);
    }else if ( type == NorahAnalytics::DB_TYPE::kUserPurchase_Backup ){
        dbPath.append(NAME_DB_BACKUP);
        tableName.append(NAME_TABLE_USERPURCHASE);
    }
    
    else if ( type == NorahAnalytics::DB_TYPE::kVirtualPurchase ){
        dbPath.append(NAME_DB_TEMP);
        tableName.append(NAME_TABLE_VIRTUALPURCHASE);
    }else if ( type == NorahAnalytics::DB_TYPE::kVirtualPurchase_Backup ){
        dbPath.append(NAME_DB_BACKUP);
        tableName.append(NAME_TABLE_VIRTUALPURCHASE);
    }
    
    sqlite3 *pdb=NULL;
    sqlite3_open(dbPath.c_str(),&pdb);
    
    std::string strSQL = StringUtils::format("select * from %s", tableName.c_str());
    char **re;
    int r,c;
    sqlite3_get_table(pdb,strSQL.c_str(),&re,&r,&c,NULL);
    sqlite3_close(pdb);
    
    return r;
}

int NorahAnalytics::getNumRowInTempDB(){
    return  getNumRowInTable(NorahAnalytics::DB_TYPE::kUserPurchase) +
    getNumRowInTable(NorahAnalytics::DB_TYPE::kUserBehaviour) +
    getNumRowInTable(NorahAnalytics::DB_TYPE::kVirtualPurchase);
}

int NorahAnalytics::getNumRowInBackupDB(){
    return  getNumRowInTable(NorahAnalytics::DB_TYPE::kUserPurchase_Backup) +
    getNumRowInTable(NorahAnalytics::DB_TYPE::kUserBehaviour_Backup) +
    getNumRowInTable(NorahAnalytics::DB_TYPE::kVirtualPurchase_Backup);
}

void NorahAnalytics::postTempDB(){
    if (getNumRowInTable(DB_TYPE::kUserBehaviour) > 0)
        postData(DB_TYPE::kUserBehaviour, getEntriesFromDatabase(DB_TYPE::kUserBehaviour) );
    else if (getNumRowInTable(NorahAnalytics::DB_TYPE::kUserBehaviour_Backup) > 0){
        postBackupData(DB_TYPE::kUserBehaviour_Backup,getEntriesFromDatabase(DB_TYPE::kUserBehaviour_Backup));
    }
    
    if (getNumRowInTable(DB_TYPE::kUserPurchase) > 0)
        postData(DB_TYPE::kUserPurchase, getEntriesFromDatabase(DB_TYPE::kUserPurchase) );
    else if ( getNumRowInTable(DB_TYPE::kUserPurchase_Backup) > 0 ){
        postBackupData(DB_TYPE::kUserPurchase_Backup,
                       getEntriesFromDatabase(DB_TYPE::kUserPurchase_Backup));
    }
    
    
    if (getNumRowInTable(DB_TYPE::kVirtualPurchase) > 0)
        postData(DB_TYPE::kVirtualPurchase, getEntriesFromDatabase(DB_TYPE::kVirtualPurchase) );
    else if ( getNumRowInTable(DB_TYPE::kVirtualPurchase_Backup) > 0 ){
        postBackupData(DB_TYPE::kVirtualPurchase_Backup,
                       getEntriesFromDatabase(DB_TYPE::kVirtualPurchase_Backup));
    }
}

void NorahAnalytics::postBackupDB(){
    if (getNumRowInTable(DB_TYPE::kUserBehaviour_Backup) > 0)
        postBackupData(DB_TYPE::kUserBehaviour_Backup, getEntriesFromDatabase(DB_TYPE::kUserBehaviour_Backup) );
    if (getNumRowInTable(DB_TYPE::kUserPurchase_Backup) > 0)
        postBackupData(DB_TYPE::kUserPurchase_Backup, getEntriesFromDatabase(DB_TYPE::kUserPurchase_Backup) );
    if (getNumRowInTable(DB_TYPE::kVirtualPurchase_Backup) > 0)
        postBackupData(DB_TYPE::kVirtualPurchase_Backup, getEntriesFromDatabase(DB_TYPE::kVirtualPurchase_Backup) );
}

void NorahAnalytics::handleEvent(NorahAnalytics::DB_TYPE type,const char* entry1){
    std::string path = FileUtils::getInstance()->getWritablePath();
    DB_TYPE typeBackup = DB_TYPE::kNone;
    if ( type == NorahAnalytics::DB_TYPE::kUserBehaviour ){
        path.append(NAME_DB_TEMP);
        typeBackup = DB_TYPE::kUserBehaviour_Backup;
    }else if ( type == DB_TYPE::kUserPurchase ){
        path.append(NAME_DB_TEMP);
        typeBackup = DB_TYPE::kUserPurchase_Backup;
    }else if ( type == DB_TYPE::kVirtualPurchase ){
        return;
    }
    
    std::string currentTime = getCurrentTime();
    std::vector<std::string> entry;
    entry.push_back(entry1);
    entry.push_back(currentTime);
    if ( getNumRowInTempDB() >= MAX_ENTRY){
        
        if (!isInternetConnected()){
#ifdef ENABLE_LOG
            _logger->setLog("WARN: NO internet connection");
            _logger->setLog("Save all entries from temp.db to backup.db");
#endif
            moveEntriesFromTempToBackup();
            return;
        }
        
        if ( (type == NorahAnalytics::DB_TYPE::kUserBehaviour && _isDB_UserBehavierLocked) ||
            (type ==  DB_TYPE::kUserPurchase && _isDB_UserPurchaseLocked) ){
#ifdef ENABLE_LOG
            _logger->setLog("WARN: temp.db is locked because there is a pending request");
            _logger->setLog(StringUtils::format( "Save entry [%s,%s] to backup.db", entry1, currentTime.c_str() ).c_str());
#endif
            saveEntryToDB(typeBackup, entry1, currentTime.c_str());
            return;
        }
        
        if (type == DB_TYPE::kUserBehaviour){
            _unhandleEntryUserBehavier.push_back(entry);
        }else if (type == DB_TYPE::kUserPurchase){
            _unhandleEntryUserPurchase.push_back(entry);
        }
        
        if ( _playerID.compare("default") == 0 ){
            requestPlayerId();
        }else{
            //read all entry and send post
            postTempDB();
        }
        
    }else{
        //save entry to db
#ifdef ENABLE_LOG
        _logger->setLog( StringUtils::format( "Save entry [%s,%s] to temp.db", entry1, currentTime.c_str() ).c_str() );
#endif
        saveEntryToDB(type, entry1, currentTime.c_str());
    }
}


void NorahAnalytics::handleEvent(NorahAnalytics::DB_TYPE type, const char* entry1, const char* entry2){
    std::string path = FileUtils::getInstance()->getWritablePath();
    std::vector<std::string> entry;
    if ( type == NorahAnalytics::DB_TYPE::kUserBehaviour ||  type == DB_TYPE::kUserPurchase){
        return;
    }
    else if ( type == DB_TYPE::kVirtualPurchase ){
    }
    
    std::string currentTime = getCurrentTime();
    path.append(NAME_DB_TEMP);
    entry.push_back(entry1);
    entry.push_back(entry2);
    entry.push_back(currentTime);
    
    
    if ( getNumRowInTempDB() >= MAX_ENTRY){
        
        if (!isInternetConnected()){
#ifdef ENABLE_LOG
            _logger->setLog("WARN: NO internet connection");
            _logger->setLog("Save all entries from temp.db to backup.db");
#endif
            moveEntriesFromTempToBackup();
            return;
        }
        
        if ( _isDB_VirtualPurchaseLocked ){
#ifdef ENABLE_LOG
            _logger->setLog("WARN: temp.db is locked because there is a pending request");
            _logger->setLog( StringUtils::format( "Save entry [%s,%s,%s] to backup.db", entry1, entry2, entry[2].c_str() ).c_str() );
#endif
            saveEntryToDB(DB_TYPE::kVirtualPurchase_Backup, entry1, entry2, currentTime.c_str());
            return;
        }
        
        _entryVirtualPurchase.push_back(entry);
        if ( _playerID.compare("default") == 0 ){
            requestPlayerId();
        }else{
            //read all entry and send post
            postTempDB();
        }
        
    }else{
        //save entry to db
#ifdef ENABLE_LOG
        _logger->setLog( StringUtils::format( "Save entry [%s,%s,%s] to temp.db", entry1, entry2, entry[2].c_str() ).c_str() );
#endif
        std::string currentTime = getCurrentTime();
        saveEntryToDB(type, entry1, entry2, currentTime.c_str());
    }
}


void NorahAnalytics::saveEntryToDB(DB_TYPE type, const char* entry, const char* timestamp){
    std::string currentTime = getCurrentTime();
    std::string strTime="";
    if (strcmp(timestamp, "currentTime") == 0){
        strTime.append( currentTime );
    }else{
        strTime.append(timestamp);
    }
    
    std::string dbPath = FileUtils::getInstance()->getWritablePath();
    std::string strSQL = "";
    if ( type == NorahAnalytics::DB_TYPE::kUserBehaviour ){
        dbPath.append(NAME_DB_TEMP);
        strSQL.append(StringUtils::format("insert into userbehaviour values('%s','%s')",
                                          entry, strTime.c_str()));
    }else if ( type == NorahAnalytics::DB_TYPE::kUserBehaviour_Backup ){
        dbPath.append(NAME_DB_BACKUP);
        strSQL.append(StringUtils::format("insert into userbehaviour values('%s','%s')",
                                          entry, strTime.c_str()));
    }
    
    else if ( type == DB_TYPE::kUserPurchase ){
        dbPath.append(NAME_DB_TEMP);
        strSQL.append(StringUtils::format("insert into userpurchase values('%s','%s')",
                                          entry, strTime.c_str()));
    }else if ( type == DB_TYPE::kUserPurchase_Backup ){
        dbPath.append(NAME_DB_BACKUP);
        strSQL.append(StringUtils::format("insert into userpurchase values('%s','%s')",
                                          entry, strTime.c_str()));
    }
    
    sqlite3 *pdb = nullptr;
    sqlite3_open(dbPath.c_str(),&pdb);
    sqlite3_exec(pdb,strSQL.c_str(),NULL,NULL,NULL);
    sqlite3_close(pdb);
}

void NorahAnalytics::saveEntryToDB(DB_TYPE type, const char* entry1, const char* entry2, const char* timestamp){
    std::string currentTime = getCurrentTime();
    std::string strTime="";
    if (strcmp(timestamp, "currentTime") == 0){
        strTime.append( currentTime );
    }else{
        strTime.append(timestamp);
    }
    
    std::string dbPath = FileUtils::getInstance()->getWritablePath();
    std::string strSQL = "";
    
    if ( type == DB_TYPE::kVirtualPurchase ){
        dbPath.append(NAME_DB_TEMP);
        strSQL.append(StringUtils::format("insert into virtualpurchase values('%s','%s','%s')",
                                          entry1, entry2, strTime.c_str() ));
        
    }else if ( type == DB_TYPE::kVirtualPurchase_Backup ){
        dbPath.append(NAME_DB_BACKUP);
        strSQL.append(StringUtils::format("insert into virtualpurchase values('%s','%s','%s')",
                                          entry1, entry2, strTime.c_str() ));
    }
    
    sqlite3 *pdb = nullptr;
    sqlite3_open(dbPath.c_str(),&pdb);
    sqlite3_exec(pdb,strSQL.c_str(),NULL,NULL,NULL);
    sqlite3_close(pdb);
}

void NorahAnalytics::saveUserInfoToDB(const char *gameId, const char *playerID, const char *system_info, const char *timestamp){
    
    
    std::string currentTime = getCurrentTime();
    std::string strTime="";
    if (strcmp(timestamp, "currentTime") == 0){
        strTime.append( currentTime );
    }else{
        strTime.append(timestamp);
    }
    
    std::string dbPath = FileUtils::getInstance()->getWritablePath();
    std::string strSQL = "";
    
    dbPath.append(NAME_DB_TEMP);
    strSQL.append(StringUtils::format("insert into user_info values('%s','%s','%s','%s')",
                                      gameId, playerID, system_info, strTime.c_str() ));
    
    
    sqlite3 *pdb = nullptr;
    sqlite3_open(dbPath.c_str(),&pdb);
    sqlite3_exec(pdb,strSQL.c_str(),NULL,NULL,NULL);
    sqlite3_close(pdb);
}

void NorahAnalytics::moveEntriesFromTempToBackup(){
    auto dbPath = FileUtils::getInstance()->getWritablePath();
    long sizeBackup =  FileUtils::getInstance()->getFileSize(dbPath + NAME_DB_BACKUP);
    if (sizeBackup >= LIMIT_FILE_SIZE){
#ifdef ENABLE_LOG
        _logger->setLog("backup.db size >= 5MB. Discard entry");
        return;
#endif
    }
    
    //copy temp to backup
    //INSERT INTO Destination SELECT * FROM Source;
    //INSERT INTO Proteins(SpeciesId, UniProtKBAccessionNumber) VALUES ((SELECT Id FROM Species WHERE ShortName = ?), ?)
    std::string tempPath = FileUtils::getInstance()->getWritablePath() + NAME_DB_TEMP;
    std::string backupPath = FileUtils::getInstance()->getWritablePath() + NAME_DB_BACKUP;
    sqlite3 *pdbTemp = nullptr;
    sqlite3 *pdbBackup = nullptr;
    sqlite3_open(tempPath.c_str(),&pdbTemp);
    sqlite3_open(backupPath.c_str(),&pdbBackup);
    
    
    //copy user behaviour from temp to backup
    std::string strSQLSelect = StringUtils::format("select * from %s", NAME_TABLE_USERBEHAVIER);
    char **re1;
    int r1,c1;
    sqlite3_get_table(pdbTemp,strSQLSelect.c_str(),&re1,&r1,&c1,NULL);
    for ( int row = 1; row <= r1; row++ ){
        std::vector<std::string> entry;
        for (int col = 0; col < c1; col++){
            entry.push_back(re1[c1*row + col]);
        }
        
        //insert to backup
        std::string strSQLInsert = StringUtils::format("insert into userbehaviour values('%s','%s')",
                                                       entry[0].c_str(), entry[1].c_str());
        sqlite3_exec(pdbBackup,strSQLInsert.c_str(),NULL,NULL,NULL);
    }
    
    //copy user purchase from temp to backup
    strSQLSelect.clear();
    strSQLSelect.append( StringUtils::format("select * from %s", NAME_TABLE_USERPURCHASE) );
    char **re2;
    int r2,c2;
    sqlite3_get_table(pdbTemp,strSQLSelect.c_str(),&re2,&r2,&c2,NULL);
    for ( int row = 1; row <= r2; row++ ){
        std::vector<std::string> entry;
        for (int col = 0; col < c2; col++){
            entry.push_back(re2[c2*row + col]);
        }
        
        //insert to backup
        std::string strSQLInsert = StringUtils::format("insert into userpurchase values('%s','%s')",
                                                       entry[0].c_str(), entry[1].c_str());
        sqlite3_exec(pdbBackup,strSQLInsert.c_str(),NULL,NULL,NULL);
    }
    
    //copy virtual purchase from temp to backup
    strSQLSelect.clear();
    strSQLSelect.append( StringUtils::format("select * from %s", NAME_TABLE_VIRTUALPURCHASE) );
    char **re3;
    int r3,c3;
    sqlite3_get_table(pdbTemp,strSQLSelect.c_str(),&re3,&r3,&c3,NULL);
    for ( int row = 1; row <= r3; row++ ){
        std::vector<std::string> entry;
        for (int col = 0; col < c3; col++){
            entry.push_back(re3[c3*row + col]);
        }
        
        //insert to backup
        std::string strSQLInsert = StringUtils::format("insert into virtualpurchase values('%s','%s','%s')",
                                                       entry[0].c_str(), entry[1].c_str(), entry[2].c_str());
        sqlite3_exec(pdbBackup,strSQLInsert.c_str(),NULL,NULL,NULL);
    }
    
    //delete temp
    sqlite3_exec(pdbTemp,"delete from userbehaviour",NULL,NULL,NULL);
    sqlite3_exec(pdbTemp,"delete from userpurchase",NULL,NULL,NULL);
    sqlite3_exec(pdbTemp,"delete from virtualpurchase",NULL,NULL,NULL);
    
    
    sqlite3_close(pdbTemp);
    sqlite3_close(pdbBackup);
}

std::string NorahAnalytics::getPlayerIdFromDatabase(){
    std::string result = "default";
    std::string dbPath = FileUtils::getInstance()->getWritablePath() + NAME_DB_TEMP;
    sqlite3 *pdb=NULL;
    sqlite3_open(dbPath.c_str(),&pdb);
    
    std::string strSQL("select playerId from user_info");
    char **re;
    int r,c;
    sqlite3_get_table(pdb,strSQL.c_str(),&re,&r,&c,NULL);
    
    if (r == 1){
        result.clear();
        result.append( re[1] );
    }
    
    sqlite3_close(pdb);
    
    return result;
}
