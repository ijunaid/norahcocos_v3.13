#ifndef __NorahAnalytics_SCENE_H__
#define __NorahAnalytics_SCENE_H__

#include "cocos2d.h"
#include "sqlite3.h"
#include "network/HttpClient.h"
#include <fstream>
#include "ConnectNative.h"

//#define ENABLE_LOG

#ifdef ENABLE_LOG
class HelloWorld;
#endif

using namespace cocos2d::network;

class NorahAnalytics
{
public:
    enum DB_TYPE {
        kUserBehaviour,
        kUserBehaviour_Backup,
        kUserPurchase,
        kUserPurchase_Backup,
        kVirtualPurchase,
        kVirtualPurchase_Backup,
        kUserInfo,
        kNone
    };
    
private:
    std::deque<std::vector<std::string>> _unhandleEntryUserBehavier;//param1 param2:timestamp
    std::deque<std::vector<std::string>> _unhandleEntryUserPurchase;
    std::deque<std::vector<std::string>> _entryVirtualPurchase;
    
    bool _isDB_UserBehavierLocked;
    bool _isDB_UserPurchaseLocked;
    bool _isDB_VirtualPurchaseLocked;
    bool _isDB_UserLocked;
    std::string _playerID;
    std::string _devideID;
    DB_TYPE _type;
    
#ifdef ENABLE_LOG
    HelloWorld * _logger;
#endif
    
public:
    NorahAnalytics();
    ~NorahAnalytics();
    
    int getNumRowInTable(DB_TYPE type);
    int getNumRowInTempDB();
    int getNumRowInBackupDB();
    void postTempDB();
    void postBackupDB();
    //save entry to database 'userbehavie'r and 'userpurchase'
    void saveEntryToDB(DB_TYPE type, const char* entry, const char* timestamp);
    void saveEntryToDB(DB_TYPE type, const char* entry1, const char* entry2, const char* timestamp);
    void saveUserInfoToDB(const char* gameId, const char* playerID, const char* system_info, const char* timestamp);
    void moveEntriesFromTempToBackup();
    
    std::string getEntriesFromDatabase(DB_TYPE type);
    void postData(DB_TYPE type, const std::string &data);
    void postBackupData(DB_TYPE type, const std::string &data);
    void postUserInfo(const char* gameId, const char* playerID, const char* system_info, const char* timestamp);
    void createDatabase(DB_TYPE type);
    void clearDatabase(DB_TYPE type);
    
    void handleEvent(DB_TYPE type, const char* entry1);
    void handleEvent(DB_TYPE type, const char* entry1, const char* entry2);
    void onHttpPostCallback(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response);
    void onHttpPostBackupCallback(HttpClient *sender, HttpResponse *response);
    std::string getCurrentTime();
    void requestPlayerId();
    void onHttprequestPlayerIdCallback(HttpClient *sender, HttpResponse *response);
    inline std::string getPlayerId(){ return _playerID; };
    bool isInternetConnected();
    std::string getPlayerIdFromDatabase();
    
#ifdef ENABLE_LOG
    inline void setLogger( HelloWorld * logger ) { _logger = logger; };
#endif
};

#endif  // __NorahAnalytics_SCENE_H__
